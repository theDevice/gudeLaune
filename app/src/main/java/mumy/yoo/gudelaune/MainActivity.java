package mumy.yoo.gudelaune;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    Button button0 = null;
    int SAMPLERATE = 44100;

    InputStream audioStream = null;

    int sampleSize = 0;

    boolean currentlyPlaying = false;
    boolean startAgain = false;

    int tempBufferSize = 999999;
    byte[] tempBuffer = new byte[tempBufferSize];
    byte[] buffer = null;

    int minBufferSize = 0;
    AudioTrack audioTrack = null;
    

    Runnable runnable = null;

    @Override
    protected  void onStop(){
        super.onStop();
        try{
            audioStream.close();
        }catch( Exception e ){

        }
    }


    @Override
    protected void onCreate( Bundle savedInstanceState ){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button0 = (Button)findViewById( R.id.button0 );
        button0.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //playAirhorn();
                startPlayback();
            }
        });
        button0.setSoundEffectsEnabled( false );

        audioStream = getApplicationContext().getResources().openRawResource( R.raw.sound );

        try{
            sampleSize = audioStream.available();
            audioStream.read( tempBuffer );
        }catch( Exception e ){
            Log.d("_debug", "exception");
        }

        buffer = new byte[sampleSize];
        for( int index = 0; index < sampleSize; index++ ){
            buffer[index] = tempBuffer[index];
        }

        addFadeIn();

        minBufferSize = AudioTrack.getMinBufferSize( SAMPLERATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT );
        audioTrack = new AudioTrack( AudioManager.STREAM_MUSIC, SAMPLERATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, minBufferSize, AudioTrack.MODE_STREAM );

        runnable = new Runnable() {
            @Override
            public void run(){
                playAirhorn();
            }
        };

    }

    /*
    private void delay( int in_delay ){
        synchronized( this ){
            try{
                wait(in_delay);
            }catch( Exception exception ){
                Log.d("_debug", " ... wait(" + in_delay + ") throwed exception: " + exception);
            }
        }
    }
    */

    public void startPlayback(){

        if( currentlyPlaying  ){
            startAgain = true;
        }else{
            Thread thread = new Thread(runnable);
            thread.start();
        }

    }

    private void playAirhorn(){

        currentlyPlaying = true;
        //playbackMutex.lock();

        audioTrack.play();
        do{
            startAgain = false;
            for (int index = 0; index * minBufferSize <= sampleSize && startAgain == false; index++) {
                audioTrack.write(buffer, index * minBufferSize, minBufferSize);
            }
        }while(startAgain);
        audioTrack.stop();

        //playbackMutex.unlock();
        currentlyPlaying = false;

    }

    private void addFadeIn(){
        int attack = 1000;
        for( int index = 0; index < attack; index++ ){
            buffer[index] = (byte)( buffer[index] * ( ( (float)(index) ) / ( (float)(attack) ) ) );
        }
    }

}
